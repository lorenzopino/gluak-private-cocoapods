Pod::Spec.new do |s|

  s.name         = "iPhoneXLibrary"
  s.version      = "2.0"
  s.summary      = "iPhone XLibrary."

  s.description  = <<-DESC
  					iPhone XLibrary Description
                   DESC

  s.homepage     = "https://bitbucket.org/gluak/xlibs-ios_2.0/wiki/Home"

  s.license      = "Commercial"

  s.author             = { "Alessandro Ventura" => "a.ventura@gluak.com" }

  s.platform     = :ios, "7.0"

  s.source = { :git => 'https://bitbucket.org/gluak/xlibs-ios_2.0.git', :tag => '2.0.5' }

  s.source_files  = "iPhone XLibrary 2.0", "iPhone XLibrary 2.0/iPhone XLibrary 2.0/**/*.{h,m}"

  s.requires_arc = true
  
  s.dependency "ASIHTTPRequest"
  s.dependency 'GoogleAnalytics'

end
